<?php 

require "../partials/template.php";
function get_title(){
echo "My Profile";
}

function get_body_contents(){
require "../controllers/connection.php";

?>
<h1 class="text-center py-5">My Profile</h1>

<div class="container">
	<div class="row">
		<!-- Profile Details -->
		<div class="col-lg-6">
			<h2 class="text-center">Welcome User</h2>
			<h4>First Name: <?php echo $_SESSION['user']['firstName'] ?></h4>
			<h4>Last Name: <?php echo $_SESSION['user']['lastName'] ?></h4>
			<h4>Last Name: <?php echo $_SESSION['user']['email'] ?></h4>
		</div>
		<div class="col-lg-6">
			<h2 class="text-center">Addresses:</h2>
			<ul>
				<?php 
				$userId = $_SESSION['user']['id'];
				$address_query = "SELECT * FROM addresses WHERE user_id = $userId";
				$addresses = mysqli_query($conn, $address_query);

				foreach($addresses as $indiv_address) {
				?>	
				<li>
				<?php echo $indiv_address['address1'] . "," . $indiv_address['address2'] . "<br>" . $indiv_address['city'] . " " . $indiv_address['zip_code']?>
				</li>
				<?php
				}
				?>
			</ul>
			<form action="../controllers/add-address-process.php" method="POST">
				<div class="form-group">
					<label for="address1">Address 1:</label>
					<input type="text" name="address1" class="form-control">
					<label for="address2">Address 2:</label>
					<input type="text" name="address2" class="form-control">
					<label for="city">City:</label>
					<input type="text" name="city" class="form-control">
					<label for="zip_code">Zip Code:</label>
					<input type="number" name="zip_code" class="form-control">
				</div>
				<input type="hidden" name="userId" value="<?php echo $userId?>">
				<button class="btn btn-info center-block" type="submit">Add Address</button>
			</form>
			<hr class="py-4 border-white">
			<h3>Contacts</h3>
			<ul>
				<?php 

				$user_Id = $_SESSION['user']['id'];
				$contacts_query = "SELECT * FROM contacts WHERE user_id = $user_Id";
				$contacts = mysqli_query($conn, $contacts_query);

				foreach($contacts as $indiv_contact) {
				?>	
				<li>
				<?php echo $indiv_contact['contactNo']?>
				</li>
				<?php
				}
				?>
			</ul>
			<form action="../controllers/add-contacts-process.php" method="POST">
				<div class="form-group">

					<label for="contactNo">
					Contact Number:
					</label>
					<input type="text" name="contactNo" class="form-control">

				</div>
				<input type="hidden" name="user_id" value="<?php echo $user_id?>">
				<button class="btn btn-info" type="submit">
				Add Contact
				</button>
			</form>
		</div>
	</div>
</div> 
<?php
}